using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pau : MonoBehaviour
{

    public GameObject[] bloques;
    public GameObject[] Minerales;
    public GameObject[] Objectos; //Cueva
    public GameObject[] Cueva;

    private GameObject bloqueActual;
    public GameObject agua;

    public int seed = 12345;
    public int seedMinerales = 55465;
    public int seedCuevas = 55465;
    public int seedCofre = 55465;
    public int seedFlor = 55465;
    public int seedEnemigo = 55465;



    //altura maxima
    public float amp = 100f;
    public float freq = 10f;

    void Start()
    {
        GenerarMapa();
    }

    private void GenerarMapa()
    {

        Vector3 pos = this.transform.position;

        float width = 100;

        for (int x = seedMinerales; x < (seedMinerales + width); x++)
        {
            float yMineral = Mathf.PerlinNoise((x / width) * freq, 5);
            yMineral = yMineral * amp;
            yMineral = Mathf.Round(yMineral);

            for (float i = yMineral; i > yMineral - 2; i--)
            {
                if (i > .4f * amp) // a esta altra sol
                {
                    if (UnityEngine.Random.Range(0, 10) > 6)
                    {
                        GameObject newBlock = Instantiate(Minerales[0], transform);
                        if (i > 0)
                            newBlock.transform.position = new Vector3(pos.x + x - seedMinerales, i - 15, 0);
                    }
                }
                else if (i > .2f * amp)
                {
                    if (UnityEngine.Random.Range(0, 10) > 8)
                    {
                        GameObject newBlock = Instantiate(Minerales[UnityEngine.Random.Range(0,1)], transform);
                        if (i > 0)
                            newBlock.transform.position = new Vector3(pos.x + x - seedMinerales, i - 15, 0);
                    }
                }
            }
        }
        //seedCuevas
        /*
        for (int x = seedCuevas; x < (seedCuevas + width); x++)
        {
            float yCueva = Mathf.PerlinNoise((x / width) * freq, 5);
            yCueva = yCueva * amp;
            yCueva = Mathf.Round(yCueva);

            for (float i = yCueva; i > yCueva - 2; i--)
            {
                if (i > .5f * amp) 
                {
                   
                    {
                        print("pintamos Cueva1");
                        GameObject newBlock1 = Instantiate(Cueva[0], transform);
                        GameObject newBlock2 = Instantiate(Cueva[0], transform);
                        GameObject newBlock3 = Instantiate(Cueva[0], transform);
                        GameObject newBlock4 = Instantiate(Cueva[0], transform);
                        GameObject newBlock5 = Instantiate(Cueva[0], transform);




                        if (i > 0) {
                            newBlock1.transform.position = new Vector3(pos.x + x - seedCuevas,        0,     0);
                            newBlock2.transform.position = new Vector3((pos.x + x - seedCuevas) +1,  0,     0);
                            newBlock3.transform.position = new Vector3((pos.x + x - seedCuevas) -1,   0,     0);
                            newBlock4.transform.position = new Vector3(pos.x + x - seedCuevas,        1,   0 );
                            newBlock5.transform.position = new Vector3(pos.x + x - seedCuevas,       -1,     0);

                        }
                    }
                }
                else if (i > .7f * amp)
                {
                  
                    {
                        print("pintamos Cueva2");
                        GameObject newBlock1 = Instantiate(Cueva[1], transform);
                        GameObject newBlock2 = Instantiate(Cueva[1], transform);
                        GameObject newBlock3 = Instantiate(Cueva[1], transform);
                        GameObject newBlock4 = Instantiate(Cueva[1], transform);
                        GameObject newBlock5 = Instantiate(Cueva[1], transform);

                        if (i > 0)
                        {
                            newBlock1.transform.position = new Vector3(pos.x + x - seedCuevas,          0, 0);
                            newBlock2.transform.position = new Vector3((pos.x + x - seedCuevas) + 1,    0, 0);
                            newBlock3.transform.position = new Vector3((pos.x + x - seedCuevas) - 1,    0, 0);
                            newBlock4.transform.position = new Vector3(pos.x + x - seedCuevas,          1, 0);
                            newBlock5.transform.position = new Vector3(pos.x + x - seedCuevas,         -1, 0);

                        }
                    }
                }
            }
        }
        */







        for (int x = 0; x < width; x++)
        {
            float y = Mathf.PerlinNoise((x / width) * freq + seed, 5);
            y = y * amp;

            
            
            y = Mathf.Round(y);


            float yCofre = Mathf.PerlinNoise((x / width) * freq + seedCofre, 5);
            if (yCofre > 0.8)
            {
                
                if (UnityEngine.Random.Range(0, 10) > 7)
                {
                    GameObject newBlock = Instantiate(Objectos[0], transform);
                    
                    newBlock.transform.position = new Vector3(pos.x + x, y+1, 0);
                }
                //print("genero un cofre " + y + " la X --> " + x);
            }

            float yFlor = Mathf.PerlinNoise((x / width) * freq + seedFlor, 5);
            if (yFlor > 0.3)
            {

                if (UnityEngine.Random.Range(0, 10) > 8)
                {
                    GameObject newBlock = Instantiate(Objectos[1], transform);

                    newBlock.transform.position = new Vector3(pos.x + x, y + 1, 0);
                }
                //print("genero una flor " + y + " la X --> " + x);
            }

            float yEnemigo = Mathf.PerlinNoise((x / width) * freq + seedEnemigo, 5);
            if (yEnemigo > 0.5)
            {

                if (UnityEngine.Random.Range(0, 10) % 2 == 0) // apetece
                {
                    GameObject newBlock = Instantiate(Objectos[2], transform);

                    newBlock.transform.position = new Vector3(pos.x + x, y + 1, 0);
                }
                //print("genero un enemigo " + y + " la X --> " + x);
            }





            for (float i = y; i >= -5; i--)
            {
                if (i > .7f * amp)
                {
                    bloqueActual = bloques[2];
                }
                else if (i > .4f * amp)
                {
                    bloqueActual = bloques[1];
                }
                else
                {
                    bloqueActual = bloques[0];
                }
                GameObject newBlock = Instantiate(bloqueActual, transform);
                newBlock.transform.position = new Vector3(pos.x + x, i, 0);
            }
            
            for (float i = y; i < 0.3f*amp; i++)
            {
                GameObject aguaPorFavor = Instantiate(agua, transform);
                aguaPorFavor.transform.position = new Vector3(pos.x + x, i, 0);
            }


            for (float i = y; i > -3; i--)
            {
                float yCueva = Mathf.PerlinNoise((x / width) * freq, i);

                if (i < .1*amp && yCueva < 0.4)
                {
                    int vueltas = UnityEngine.Random.Range(2, 5);
                    int acc = 0;
                    for (int j = 0; j<vueltas; j++)
                    {
                        GameObject newBlock1 = Instantiate(Cueva[1], transform);
                        newBlock1.transform.position = new Vector3(pos.x + x, i+acc, 0);
                        acc++;
                    }
                }
            }



        }
        

    }
}
