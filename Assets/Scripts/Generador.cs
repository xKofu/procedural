using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Generador : MonoBehaviour
{
    //Guarda

    public TileBase[] tiles;
    public TileBase[] Minerales;
    public TileBase[] Objectos;
    public GameObject agua;
    public Tilemap tilemap;
    public int seed = 12345;
    public int seedMinerales = 55465;
    public int seedCofre = 55465;
    public int seedFlor = 4587;

    //altura maxima
    public float amp = 100f;
    public float freq = 10f;

    void Start()
    {
        GenerarMapa();
    }

    private void GenerarMapa()
    {

        TileBase TileActual;

        Vector3 pos = this.transform.position;

        float width = 250f;
        
        
            for (int x = 0; x < width; x++) {

            float y = Mathf.PerlinNoise((x / width) * freq + seed, 5);
            y = y * amp;

            y = Mathf.Round(y);

            // GENERACION COFRES

            float yCofre = Mathf.PerlinNoise((x / width) * freq + seedCofre, 5);
            if (yCofre > 0.7)
            {

                if (UnityEngine.Random.Range(0, 10) > 7)
                {
                    tilemap.SetTile(new Vector3Int(x, (int)y + 1, 0), Objectos[0]);
                }
                
            }

            // GENERACION FLORES

            float yFlor = Mathf.PerlinNoise((x / width) * freq + seedFlor, 5);
            if (yFlor > 0.3)
            {

                if (UnityEngine.Random.Range(0, 10) > 8)
                {
                    tilemap.SetTile(new Vector3Int(x, (int)y + 1, 0), Objectos[1]);
                }
            }

            for (int i = (int)y; i >= -3; i--)
            {
                if (i == (int)y) {
                    TileActual = tiles[2];
                } 
                else if (i > .2f * amp)
                {
                    TileActual = tiles[1];
                }
                else
                {
                    float yMineral = Mathf.PerlinNoise((x / width) * freq + seedMinerales, i * freq + seedMinerales);
                    if (i < 3 && yMineral > .6f)
                    {
                    TileActual = Minerales[UnityEngine.Random.Range(0, Minerales.Length)];
                    } else
                    {
                    TileActual = tiles[0];
                    }

                }
                tilemap.SetTile(new Vector3Int(x, i, 0), TileActual);
            }

            // GENERACION CUEVAS

            for (float i = y; i > -3; i--)
            {
                float yCueva = Mathf.PerlinNoise((x / width) * freq, i);

                // i < .1 * amp 

                if (i < .1 * amp && yCueva < 0.4)
                {
                    int vueltas = UnityEngine.Random.Range(2, 5);
                    int acc = 0;
                    for (int j = 0; j < vueltas; j++)
                    {
                        tilemap.SetTile(new Vector3Int((int)pos.x + x, (int)i + acc, 0), null);
                        acc++;
                    }
                }
            }


        }


    }
}
